# Terraform Template

A template for my Terraform needs.

## Requirements

- `terraform`, obviously
- `just`

## Usage

1. Create a repo from the template
2. Update `src/terraform.tf`
   1. Set the `bucket`
   2. Add `required_providers`
3. Copy `.env.example` to `.env`, and add credentials
4. Add credentials to Gitea
