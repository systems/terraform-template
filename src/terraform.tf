terraform {
  backend "s3" {
    bucket = "terraform-template"
    key    = "terraform.tfstate"
    region = "main"

    endpoints = {
      s3 = "https://s3.jakehoward.tech"
    }

    skip_region_validation      = true
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    use_path_style              = true
  }

  required_providers {}
}
