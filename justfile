# Run terraform with required environment
terraform +ARGS:
    #!/usr/bin/env bash

    # Load secrets from env file (if it exists)
    set -a
    source ./.env || true
    set +a

    cd src/

    terraform {{ ARGS }}

terraform-lint:
    just terraform validate
    just terraform fmt -check -recursive
